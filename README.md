![Webparts](https://i.ibb.co/sRpwQD2/logo-webparts-branco.png)
========  
Teste para avaliar as habilidades dos candidatos a Desenvolvedor Magento.

# Introdução
# Este teste tem como objetivo avaliar seu conhecimento como desenvolvedor Magento. O teste de avaliação avaliará suas habilidades em:
  - Seu nível de conhecimento da linguagem PHP OOP (PHP versão 7 +)
  - Arquitetura de componentes Magento (Magento Open Source / Commerce 2.3+)
- Conhecimento dos principais padrões usados pela plataforma
- Uso de dependency injection
- Plugins Magento (Interceptors)
- Padrões de código e boas práticas de programação
- Princípios SOLID e recomendações de padrões PHP (PSR)
- Composer
- Docker (diferencial)

# O que será avaliado?

 - Estrutura e organização de código e arquivos
 - Soluções adotadas
 - Tecnologias utilizadas
 - Qualidade
 - PSR Patterns, Design Patterns

# Como publicar o seu teste?

 - Criar um repositório privado no Bitbucket 
 - Criar uma instalação básica do Magento 2.3+ e publicar, caso tenha escolhido usar o Docker publicar também toda a configuração (docker-compose YAML)
 - Quando terminar, enviar o link do clone do projeto para **veronicaoliveira@wpartsonline.com.br**

# Instruções

O foco principal do nosso teste de avaliação é avaliar suas habilidades e conhecimentos como desenvolvedor Magento. Depois disso faremos uma bato papo tech com nosso time. 

Você tem total liberdade para aumentar alguns conhecimentos que achar interessantes para aplicar na avaliação.

**Não há problema se você não conseguir executar todos os recursos da avaliação, faça o máximo que puder.**

# Testes - Aqui existem 6 opções de teste - Você deve escolher uma opção para entregar o seu melhor:



**Teste 1**

1. Criar um produto chamado produtoteste com o SKU "12345" valor R$100 e quantidade 10;
2. Criar um módulo chamado Webparts_TesteMagento;
3. O módulo deve rodar junto ao Cron Job do Magento a cada 5 minutos;
4. A cada vez que for executado o Cron deve pegar o produto criado na etapa 1 e incrementar R$10 e 1 quantidade ao estoque;
5. Criar um arquivo de LOG onde deverá ser gravado as informações SKU, valor e quantidade a cada execução do Cron;


**Teste 2**

1. Criar um módulo chamado Webparts_AtributoTeste
2. Ao instalar deverá criar um atributo de produto chamado "atributo_teste" associado ao grupo de atributo "default"
3. Deve ser obrigatorio do tipo input text 
4. Deve aparecer no filtro da grid de produtos no admin
5. Deve ser mostrado o valor na pagina de produto no frontend


**Teste 3**

1. Criar um módulo chamado Webparts_Pagamento
2. Criar um campo para adicionar o CPF
3. Vai ser necessário validar o CPF preenchido - campo obrigatório e válido
4. Deve aparecer o CPF no pedido efetuado
5. Deve aparecer no checkout somente se o valor do carrinho for maior que 100,00


**Teste 4**

1. Criar um módulo chamado Webparts_Frete
2. Deve aparecer no checkout somente se o valor do carrinho for maior que 20,00 e menor que 200,00
3. Permitir o frete somente para o CEP do Estado de SP 
4. O Valor do frete deve ser 20,00 + 10% do valor do carrinho


**Teste 5**

1. Criar um módulo chamado Webparts_Api
2. Criar um REST ndpoint público no padrão do Magento do método PUT
3. Passar o número do pedido no body em json
4. Deve cancelar o pedido.
5. Retornar o status 200 quando for executado com sucesso
6. Retornar o status 405 quando houver falha no processo.

**Teste 6**

 1. Criar um produto chamado produtoteste com o SKU "12345" valor R$100 e quantidade 10;
 2. Criar um módulo chamado Webparts_TesteMagento;
 3. O módulo deve rodar junto ao Cron Job do Magento a cada 5 minutos;
 4. A cada vez que for executado o Cron deve pegar o produto criado na etapa 1 e incrementar R$10 e 1 quantidade ao estoque;
 5. Criar um arquivo de LOG onde deverá ser gravado as informações SKU, valor e quantidade a cada execução do Cron;
 6. Adicionar na página do carrinho junto ao input de quantidade de cada produto dois botões "+" e "-"
 7. Ao clicar no botão "+" a quantidade do produto deve ser incrementada;
 8. Ao clicar no botão "-" a quantidade do produto deve ser decrementada;
 9. A cada clique do usuário deve apresentar uma imagem de loading e a página deve ser atualizada com a nova quantidade e total do carrinho (podendo ser via refresh ou não)


**Desejo a você sucesso! Qualquer dúvida entre em contato conosco! **
